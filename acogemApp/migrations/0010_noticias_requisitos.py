# Generated by Django 3.0.5 on 2020-09-11 16:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acogemApp', '0009_auto_20200911_1759'),
    ]

    operations = [
        migrations.AddField(
            model_name='noticias',
            name='requisitos',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
