# Generated by Django 3.0.5 on 2020-11-28 12:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acogemApp', '0013_auto_20200919_2256'),
    ]

    operations = [
        migrations.AddField(
            model_name='noticias',
            name='banco',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
