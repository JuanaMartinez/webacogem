from django.contrib import admin
from acogemApp.models import EntregaAlimentos,Noticias,Galeria,Formacion,banner,alimento,bolsaT,asesoria,acercaUs,contactos,ropero,fotoscard

# Register your models here.
class fechasEntrega(admin.ModelAdmin):
    list_display=("grupos","letras","fechaEntrega")
    
class noticiasAdmin(admin.ModelAdmin):
    list_display=("inscripciones","requisitos","OtrosAnuncios","nombreBanco","banco")

class eventosAdmin(admin.ModelAdmin):
    list_display=("descripcionFoto", "imagen","fechaEvento")

class formacionAdmin(admin.ModelAdmin):
    list_display=("despcripcionFormacion","imagenFormacion")

class bannerAdmin(admin.ModelAdmin):
    list_display=("fechacreacion","imagenesBanner")

class fotoscardAdmin(admin.ModelAdmin):
    list_display=("fechacreacion","imagenT","imagenAIndex","imagenAsesoria","imagenBolsatrabajo")

class alimentoAdmin(admin.ModelAdmin):
    list_display=("requisito_carta","requisito_padron","requisito_documents","distrito","imagenA","fechacreacion")

class bolsaTAdmin(admin.ModelAdmin):
    list_display=("requisito_bolsaEmpleos","imagenB","fechacreacion")


class asesoriaAdmin(admin.ModelAdmin):
    list_display=("fechacreacion","imagenAs")

class acercaAdmin(admin.ModelAdmin):
    list_display=("mision","vision","imagenUs")

class contactosAdmin(admin.ModelAdmin):
    list_display=("correo","telefono","direccion","horario")

class roperoAdmin(admin.ModelAdmin):
    list_display=("horarioAtencion","imagenRopero")


admin.site.register(EntregaAlimentos,fechasEntrega)
admin.site.register(Noticias,noticiasAdmin)
admin.site.register(Galeria, eventosAdmin)
admin.site.register(Formacion,formacionAdmin)
admin.site.register(banner,bannerAdmin)
admin.site.register(fotoscard,fotoscardAdmin)
admin.site.register(alimento,alimentoAdmin)
admin.site.register(bolsaT,bolsaTAdmin)
admin.site.register(asesoria,asesoriaAdmin)
admin.site.register(acercaUs,acercaAdmin)
admin.site.register(contactos,contactosAdmin)
admin.site.register(ropero,roperoAdmin)
