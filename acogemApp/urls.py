from django.urls import path
from acogemApp.views import *
from acogemApp import views


urlpatterns=[   
    path('',Home.as_view(), name='Index'),  
    path('about', about.as_view(), name='About'),
    path('bolsaEmpleo', Empleo.as_view(), name='BolsaEmpleo'),
    path('alimentos',EntregaFEGA.as_view(), name='Alimentos'),
    path('asesoria', asesoria_1.as_view(), name='AsesoriaJuridica'),
    path('contacto', contacto.as_view(), name='Contacto'),
    path('inscripciones', Inscripciones.as_view(), name='Inscripciones'),
    path('eventos',Eventos.as_view(),name='Eventos'),
    path('talleres', FormacionOcupacional.as_view(), name='Talleres'),
    path('ropero', roperos.as_view(), name='EntregaRopa'),
    path('anuncio',anuncio.as_view(), name='Anuncio'),
    path('cookie-policy',cookies_p.as_view(), name='Cookies'),
    path('bases', bases.as_view(), name='base')

]
