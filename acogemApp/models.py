from django.db import models
from datetime import date

# Create your models here.
class EntregaAlimentos(models.Model):
    grupos= models.CharField(max_length=1, null=True,blank=True)
    letras=models.CharField(max_length=50, null=True,blank=True)
    fechaEntrega=models.DateField()
    
class Noticias(models.Model):
    inscripciones = models.CharField(max_length=500,null=True, blank=True)
    requisitos= models.CharField(max_length=500,null=True, blank=True)
    OtrosAnuncios=models.CharField(max_length=500,null=True, blank=True)
    nombreBanco=models.CharField(max_length=300,null=True,blank=True)
    banco=models.CharField(max_length=500,null=True,blank=True)
    

class Galeria(models.Model):
    descripcionFoto=models.CharField(max_length=500,null=True, blank=True)
    imagen= models.ImageField(upload_to='eventos', null=True)
    fechaEvento=models.DateField(auto_now_add=True)

  
class Formacion(models.Model):
    despcripcionFormacion= models.CharField(max_length=500, null=True, blank=True)
    imagenFormacion= models.ImageField(upload_to='formacion', null=True)

class banner(models.Model):
    imagenesBanner= models.ImageField(upload_to='banner', null=True)    
    fechacreacion= models.DateField(auto_now_add=True)
    

class fotoscard(models.Model):
    imagenT=models.ImageField(upload_to='talleres', null=True)
    imagenAIndex=models.ImageField(upload_to='talleres', null=True)
    imagenAsesoria=models.ImageField(upload_to='talleres', null=True)
    imagenBolsatrabajo=models.ImageField(upload_to='talleres', null=True)
    fechacreacion=models.DateField(auto_now_add=True)

class alimento(models.Model):
    imagenA=models.ImageField(upload_to='alimentos', null=True)
    fechacreacion=models.DateField(auto_now_add=True)
    requisito_carta=models.CharField(max_length=500,null=True, blank=True)
    requisito_padron=models.CharField(max_length=500,null=True, blank=True)
    requisito_documents=models.CharField(max_length=500,null=True, blank=True)
    distrito=models.CharField(max_length=500,null=True, blank=True)

class bolsaT(models.Model):
    requisito_bolsaEmpleos=models.CharField(max_length=500,null=True, blank=True)
    imagenB=models.ImageField(upload_to='bolsaT', null=True)
    fechacreacion=models.DateField(auto_now_add=True)

class asesoria(models.Model):
    imagenAs=models.ImageField(upload_to='asesoria', null=True)
    fechacreacion=models.DateField(auto_now_add=True)

class acercaUs(models.Model):
    imagenUs=models.ImageField(upload_to='nosotros',null=True)
    mision=models.TextField(null=True,blank=True)
    vision=models.TextField(null=True,blank=True)

class contactos(models.Model):
    correo=models.CharField(max_length=300, null=True, blank=True)
    telefono=models.CharField(max_length=50, null=True, blank=True)
    direccion=models.CharField(max_length=500, null=True, blank=True)
    horario=models.TextField(null=True, blank=True)

class ropero(models.Model):
    horarioAtencion=models.TextField(null=True, blank=True)
    imagenRopero=models.ImageField(upload_to='ropero',null=True)




    
    

    