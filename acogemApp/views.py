from django.shortcuts import render,HttpResponse
from django.views.generic import View,ListView,UpdateView,TemplateView,DetailView
from django.core.paginator import Paginator
from acogemApp.models import EntregaAlimentos,Noticias,Galeria,Formacion,banner,asesoria,alimento,bolsaT,acercaUs,contactos,ropero,fotoscard


# Create your views here.
class Publicacion(ListView):    
    model= Noticias
    context_object_name='comunicacion'
    queryset = Noticias.objects.all() 

class bases(TemplateView):
    template_name='acogemApp/base.html'

    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()
        return{'correo': correo}

class anuncio(Publicacion):
    template_name='acogemApp/anuncio.html'

class cookies_p(Publicacion):    
    template_name='acogemApp/cookie-policy.html'

class asesoria_1(TemplateView):
    template_name='acogemApp/asesoriaJuridica.html'

    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()
        imagenAsesoria = asesoria.objects.all()
        return{'correo': correo,'imagenAsesoria':imagenAsesoria}


class contacto(TemplateView):
    template_name='acogemApp/contacto.html'

    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()
        return{'correo': correo}


class roperos(TemplateView):
    template_name='acogemApp/ropero.html'

    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()
        atencion= ropero.objects.all()
        return{'correo': correo,'atencion':atencion}

class about(TemplateView):   
    template_name='acogemApp/about.html'

    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()
        aboutUs= acercaUs.objects.all()
        return{'correo': correo,'aboutUs':aboutUs}

   
class Home(TemplateView):     
   paginate_by=4  
   template_name='acogemApp/index.html'
  
   def get_context_data(self,*args, **kwargs):
              
       carruselF = banner.objects.all().order_by('-fechacreacion')
       otrosanuncios  =Noticias.objects.all()
       correo  =contactos.objects.all()
       fotocards = fotoscard.objects.all().order_by('-fechacreacion')      
       fotobolsaT= bolsaT.objects.all().order_by('-fechacreacion')
       fotoasesoria = asesoria.objects.all().order_by('-fechacreacion')

       return {'carruselF': carruselF,'otrosanuncios': otrosanuncios,'fotobolsaT':fotobolsaT,'fotoasesoria':fotoasesoria,'correo': correo,'fotocards':fotocards}  
  

class EntregaFEGA(Publicacion):
    template_name='acogemApp/alimentos.html'
    
    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()  
        foods= alimento.objects.all()      
        return{'correo': correo,'foods':foods}
    
class Empleo(Publicacion):
    template_name='acogemApp/bolsaEmpleo.html'
    
    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()
        requisitos=bolsaT.objects.all()
        return{'correo': correo,'requisitos':requisitos}
    
class Inscripciones(Publicacion):
    template_name='acogemApp/inscripciones.html'
    
    def get_context_data   (self,*args,**kwargs):
        correo = contactos.objects.all()
        inscripcion= Noticias.objects.all()
        return{'correo': correo, 'inscripcion':inscripcion}

class Eventos(ListView):     
    model= Galeria     
    template_name='acogemApp/eventos.html'
    paginate_by=9    
    context_object_name= 'galerias' 
    queryset=Galeria.objects.all().order_by('-fechaEvento')
      

    def get_context_data(self, **kwargs):
        context = super(Eventos,self).get_context_data(**kwargs)      
        context['correo'] = contactos.objects.all()
        return context

    
class FormacionOcupacional(TemplateView):   
    template_name = 'acogemApp/talleres.html'

    def get_context_data(self, *args,**kwargs):
         correo  = contactos.objects.all()
         formaciones= Formacion.objects.all()

         return {'correo': correo,'formaciones':formaciones}
    



    
