"""
import sys
def application(environ, start_response):
    output = '!Hola mundo!\n\nVersion Python: %s' % sys.version

    if sys.version_info.major >= 3:
        output =  bytes(output, encoding="UTF-8")

    status = '200 OK'
    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', len(output))]
    start_response(status, response_headers)

    return [output]"""

import sys, os
sys.path.append("/home/acogemor/python/acogemApp") # I used the actual path in my file
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'WebAcogem.settings')

application = get_wsgi_application()
